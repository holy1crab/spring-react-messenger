var webpack = require('webpack');

module.exports = {
    context: __dirname + '/src/js',
    entry: [
        './main.js'
    ],
    resolve: {
        root: __dirname + '/src/js'
    },
    output: {
        path: __dirname + '/resources/js',
        filename: 'bundle.js',
        sourceMapFilename: '[file].map'
    },
    module: {
        loaders: [
            { test: /\.js?$/, loaders: ['react-hot', 'babel'], exclude: /node_modules/ },
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'},
            { test: /\.css$/, loader: "style!css" }
        ]
    },
    devtool: 'source-map',
    plugins: [
        new webpack.NoErrorsPlugin()
    ]

};
