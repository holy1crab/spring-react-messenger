import React from 'react';
import { Navigation } from 'react-router';
import MessageService from 'inbox/service/message';
import User from 'auth/model/user';
import Modal from 'ui/modal';
import { appendBody } from 'util/dom';
import { dateFormat } from 'util/common';

var service = new MessageService();

var MessageList = React.createClass({

    mixins: [Navigation],

    onRowClick(el){
        this.transitionTo('/inbox/' + el.id, null, { item: el });
    },

    onButtonRemoveClick(el, e){

        e.stopPropagation();

        service.remove(el.id, () => {
            el.deleted = true;
            this.forceUpdate();
        });
    },

    render(){

        var rows = this.props.data.map(el => {
            return (
                <tr className={'message' + (el.deleted ? ' danger' : '')} key={el.id} onClick={this.onRowClick.bind(this, el)}>
                    <td>{el.ownerFullName}</td>
                    <td>{dateFormat(new Date(el.creationDate))}</td>
                    <td>{el.subject}</td>
                    { User.current.hasRole('ROLE_ADMIN') && <td>{el.recipientFullName}</td> }
                    <td>
                        { !el.deleted && <button className="btn glyphicon glyphicon-remove" onClick={this.onButtonRemoveClick.bind(this, el)}></button> }
                    </td>
                </tr>
            )
        });

        return (
            <tbody>{rows}</tbody>
        )
    }

});

export default MessageList;