import React from 'react';
import { Navigation } from 'react-router';
import MessageService from '../service/message';
import ContactService from '../service/contact';
import MessageList from './message-list';
import User from 'auth/model/user';
import EventBus from 'util/simple-event-bus';

var messageService = new MessageService();
var contactService = new ContactService();

export default React.createClass({

    mixins: [Navigation],

    getInitialState(){
        return { data: [], contacts: [], users: [], sort: {} };
    },

    refreshContactList(){

        contactService.getContacts((response) => {

            var contacts = response.data.contacts;
            var contactIdMap = {};

            contacts.forEach((el) => contactIdMap[el.id] = el);

            contactService.getAvailable(response => {

                var users = response.data.available.filter((el) => !contactIdMap.hasOwnProperty(el.id) && el.id !== User.current.id);

                this.setState({ users: users, contacts: contacts })
            });
        });
    },

    refreshMessageList(){

        messageService.list(this.state.sort, data => {

            this.setState({ data: data });

            EventBus.instance.trigger('messages:received', { data: data });
        });
    },

    componentDidMount(){
        this.refreshMessageList();
        this.refreshContactList();

        this._timer = setInterval(this.refreshMessageList, 15 * 1000);

        this._domCurrentSortButton = null;
    },

    componentWillUnmount(){
        clearInterval(this._timer);
    },

    handleAddContact(){
        var contactId = React.findDOMNode(this.refs.available).value;
        contactService.toggle('add', contactId, (response) => this.refreshContactList());
    },

    handleDeleteContact(id){
        contactService.toggle('delete', id, (response) => this.refreshContactList());
    },

    handleToggleNewMessage(el){
        this.transitionTo('/inbox/new', { recipient: el.id });
    },

    handleSortClick(field, order, event){

        if(this._domCurrentSortButton){
            this._domCurrentSortButton.classList.remove('active');
        }

        event.target.classList.add('active');

        this._domCurrentSortButton = event.target;

        this.state.sort = [[field, order]];

        this.refreshMessageList();
    },

    render(){

        let columns = [
            { field: 'ownerFullName', title: 'От кого' },
            { field: 'creationDate', title: 'Дата' },
            { field: 'subject', title: 'Тема' }
        ];

        if(User.current.hasRole('ROLE_ADMIN')){
            columns.push({ field: 'recipientFullName', title: 'Кому' });
        }

        columns.push({ title: '' });

        var header = columns.map((col, i) => {
            return (
                <th key={i}>
                    {col.title}
                    { col.field &&
                        (
                            <span style={{ marginLeft: 10 }}>
                                <button className="btn btn-xs glyphicon glyphicon-arrow-down"
                                    style={{ border: 'none', background: 'transparent' }}
                                    onClick={this.handleSortClick.bind(this, col.field, 1)}>
                                </button>
                                <button className="btn btn-xs glyphicon glyphicon-arrow-up"
                                        style={{ border: 'none', background: 'transparent' }}
                                        onClick={this.handleSortClick.bind(this, col.field, 0)}>
                                </button>
                            </span>
                        )
                    }
                </th>
            )
        });

        var contacts = this.state.contacts.map((el) => {
            return (
                <div key={el.id} style={{ display: 'inline-block', marginRight: 10 }}>
                    <button type="button" className="btn btn-default" aria-label="Left Align" onClick={this.handleToggleNewMessage.bind(this, el)}>
                        <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span> {el.fullName}
                    </button>
                    <button className="glyphicon glyphicon-remove" onClick={this.handleDeleteContact.bind(this, el.id)}></button>
                </div>
            )
        });

        var available = this.state.users.map((el) => <option key={el.id} value={el.id}>{el.fullName}</option>);

        return (
            <div className="inbox">
                <table className="table table-striped table-hover">
                    <thead>
                        <tr>{header}</tr>
                    </thead>

                    <MessageList data={this.state.data} />
                </table>

                <div style={{ marginTop: 20 }}>
                    {contacts}
                </div>

                <div style={{ marginTop: 20 }}>
                    <select ref="available" className="form-control" style={{ display: 'inline-block', width: 200 }}>{available}</select>
                    <button className="btn" onClick={this.handleAddContact}>Add</button>
                </div>

                {this.props.children}
            </div>
        )
    }
})