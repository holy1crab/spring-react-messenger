import React from 'react';
import { Navigation } from 'react-router';
import MessageService from 'inbox/service/message';
import Modal from 'ui/modal';
import User from 'auth/model/user';

var service = new MessageService();

export default React.createClass({

    mixins: [Navigation],

    getInitialState(){
        return {
            loading: false,
            error: ''
        }
    },

    onModalRequestClose(){
        this.transitionTo('/inbox');
    },

    handleSend(){

        var subject = React.findDOMNode(this.refs.subject).value;
        var text = React.findDOMNode(this.refs.text).value;
        var recipientId = this.props.location.query.recipient;

        if(subject && text && recipientId){

            this.setState({ loading: true });

            service.send(recipientId, subject, text, (response) => {
                if(response.success){
                    this.transitionTo('/inbox');
                } else {
                    this.setState({ error: response.error.message });
                }
            });
        } else {
            this.setState({ error: 'Invalid input' });
        }
    },

    render(){

        return (
            <Modal headerText="New message" onRequestClose={this.onModalRequestClose}>

                <form>
                    <div className="form-group">
                        <label>Subject</label>
                        <input type="text" className="form-control" ref="subject" />
                    </div>
                    <div className="form-group">
                        <label>Body</label>
                        <textarea className="form-control" ref="text" />
                    </div>

                    {
                        this.state.error && (
                            <div className="alert alert-danger col-sm-12">abc</div>
                        )
                    }

                    <button type="button" className="btn btn-default" disabled={this.state.loading} onClick={this.handleSend}>Send</button>
                </form>
            </Modal>
        )
    }

});
