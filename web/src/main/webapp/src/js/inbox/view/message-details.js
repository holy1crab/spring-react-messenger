import React from 'react';
import Modal from 'ui/modal';
import MessageService from 'inbox/service/message';
import { dateFormat } from 'util/common';
import { Navigation } from 'react-router';

var service = new MessageService();

export default React.createClass({

    mixins: [Navigation],

    getInitialState(){
        return {
            item: null
        }
    },

    componentDidMount(){
        var id = parseInt(this.props.params.id, 10);
        service.list([], data => this.setState({ item: data.filter((el) => el.id === id)[0] }));
    },

    handleClose(){
        this.transitionTo('/inbox');
    },

    render(){

        let el = this.state.item;
        let subject = this.state.item ? this.state.item.subject : '';

        return (
            el &&
            <Modal headerText={subject} onRequestClose={this.handleClose}>
                <div>
                    <blockquote>
                        <p>{el.text}</p>
                        <footer>from <cite title={el.ownerFullName}>{el.ownerFullName}</cite> {dateFormat(new Date(el.creationDate))}</footer>
                    </blockquote>
                </div>
            </Modal>
        )

    }
})