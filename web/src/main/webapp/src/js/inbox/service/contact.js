import Service from 'service';

class ContactService extends Service {

    getAvailable(callback){

        this.request('/user/contact/available', {
            success: callback
        });
    }

    getContacts(callback){

        this.request('/contact/list', {
            success: callback
        });
    }

    toggle(type, contactId, callback){

        this.request('/contact/' + type, {
            method: 'POST',
            success: callback,
            data: { contactId: contactId }
        });
    }
}

export default ContactService;
