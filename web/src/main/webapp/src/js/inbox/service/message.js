import Service from 'service';

class MessageService extends Service {

    /**
     * @param {Array} sort
     * @param {Function} callback
     */
    list(sort, callback){

        this.request('/message/filter', {
            data: {
                sort: sort.length ? sort.map(el => el.join(',')).join(',') : null
            },
            success: function(response){
                callback(response.data);
            }
        });
    }

    remove(id, callback){

        this.request('/message/' + id + '/delete', {
            method: 'delete',
            success: callback
        });
    }

    send(recipientId, subject, text, callback){

        this.request('/message/send', {
            method: 'POST',
            data: { recipientId: recipientId, subject: subject, text: text },
            success: callback
        });
    }

}

export default MessageService;