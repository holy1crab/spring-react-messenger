import React from 'react';
import { Navigation, State } from 'react-router';
import AuthService from '../service/auth';
import User from 'auth/model/user';
import EventBus from 'util/simple-event-bus';

var service = new AuthService();

var LoginForm = React.createClass({

    mixins: [ Navigation, State ],

    getInitialState() {
        return {
            error: false
        };
    },

    onButtonLoginClick(e){

        e.preventDefault();

        var username = React.findDOMNode(this.refs.username).value.trim();
        var password = React.findDOMNode(this.refs.password).value.trim();

        service.login(username, password, (success) => {

            if(!success){
                return this.setState({ error: true });
            }

            service.check((data) => {

                User.current.setId(data.id);
                User.current.setFullName(data.fullName);
                User.current.setRoles(data.roles);

                EventBus.instance.trigger('authenticated');

                var { location } = this.context.router.state;

                if (location.query && location.query.next) {
                    this.replaceWith(location.query.next);
                } else {
                    this.replaceWith('/');
                }
            });
        });
    },

    onChange(){
        this.setState({ error: false });
    },

    render(){

        return (
            <div id="loginbox" className="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <div className="panel panel-info">
                    <div className="panel-heading">
                        <div className="panel-title">Sign In</div>
                    </div>

                    <div style={{paddingTop: 30}} className="panel-body">

                        <form id="loginform" className="form-horizontal" role="form">

                            <div style={{marginBottom: 25}} className="input-group">
                                <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                                <input id="login-username" type="text" className="form-control" name="username" placeholder="login" ref="username" onChange={this.onChange} />
                            </div>

                            <div style={{marginBottom: 25}} className="input-group">
                                <span className="input-group-addon"><i className="glyphicon glyphicon-lock"></i></span>
                                <input id="login-password" type="password" className="form-control" name="password" placeholder="password" ref="password" onChange={this.onChange} />
                            </div>

                            {
                                this.state.error && (
                                    <div id="login-alert" className="alert alert-danger col-sm-12">Invalid credentials</div>
                                )
                            }

                            <div style={{marginTop: 10}} className="form-group">
                                <div className="col-sm-12 controls">
                                    <button type="button" id="btn-login" className="btn btn-success" onClick={this.onButtonLoginClick}>Login </button>
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="col-md-12 control">
                                    <div style={{borderTop: 1, solid: '#888', paddingTop: 15, fontSize: '85%'}}>
                                        Don't have an account!
                                        <a href="#" id="sign-up-toggle" style={{paddingLeft: 5}} onClick={this.props.onButtonToggleRegistrationClick}>Sign Up Here</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        )

    }
});

export default LoginForm;