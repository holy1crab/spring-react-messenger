import React from 'react';
import LoginForm from './login-form';
import RegistrationForm from './registration-form';
import { Navigation, State } from 'react-router';

import User from 'auth/model/user';

export default React.createClass({

    mixins: [Navigation, State],

    handleToggleRegister(e) {

        e.preventDefault();

        this.transitionTo('/register');
    },

    render() {

        return (
            <div>
                {
                    (User.current.isAuthenticated())
                    ? <div>Already logged in</div>
                    : <LoginForm onButtonToggleRegistrationClick={this.handleToggleRegister}/>
                }
            </div>
        )
    }
})