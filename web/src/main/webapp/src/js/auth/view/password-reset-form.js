import React from 'react';
import { Navigation } from 'react-router';
import Modal from 'ui/modal';
import AuthService from 'auth/service/auth';

var authService = new AuthService();

export default React.createClass({

    mixins: [Navigation],

    getInitialState(){

        return {
            error: '',
            success: false
        }
    },

    onButtonChangePasswordClick(){

        var oldPassword = React.findDOMNode(this.refs.oldPassword);
        var newPassword = React.findDOMNode(this.refs.newPassword);

        authService.changePassword(oldPassword.value, newPassword.value, (response) => {

            if(response.success){
                this.setState({ success: true });
            } else {
                this.setState({ error: response.error.message });
            }
        })
    },

    render(){

        return (
            <Modal headerText="Change password">
                {
                    !this.state.success
                    ?    <form>
                            <div className="form-group">
                                <label>Old password</label>
                                <input type="password" className="form-control" ref="oldPassword" onInput={this.onInput}/>
                            </div>
                            <div className="form-group">
                                <label>New password</label>
                                <input type="password" className="form-control" ref="newPassword"/>
                            </div>

                            {this.state.error && <div id="login-alert" className="alert alert-danger col-sm-12">{this.state.error}</div>}

                            <button type="button" className="btn btn-default" onClick={this.onButtonChangePasswordClick}>Save</button>
                        </form>

                    :   <div>Password successfully changed</div>
                }
            </Modal>
        )
    }

})