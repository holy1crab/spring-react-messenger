import React from 'react';
import AuthService from 'auth/service/auth';

var authService = new AuthService();

class RegistrationForm extends React.Component {

    constructor(props){

        super(props);

        this.state = {
            loading: false,
            error: ''
        };
    }

    onButtonSignUpClick(){

        var values = {};

        this.props.fields.forEach((el) => values[el.ref] = React.findDOMNode(this.refs[el.ref]).value, this);

        this.setState({ loading: true });

        authService.register({ data: values }, (response) => {

            if(response.error){
                this.setState({ error: response.error.message, loading: false });
            } else {
                this.props.onRegister(response.data.user, values.password);
            }
        })
    }

    render() {

        return (
            <div className="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

                <div className="panel panel-info">
                    <div className="panel-heading">
                        <div className="panel-title">Sign Up</div>
                        <div style={{float: 'right', fontSize: '85%', position: 'relative', top: '-10px'}}>
                            <a id="sign-in-toggle" onClick={this.props.onButtonToggleAuthClick}>Sign In</a>
                        </div>
                    </div>
                    <div className="panel-body">
                        <form className="form-horizontal" role="form">
                            {
                                this.props.fields.map(function(el, i){
                                    return (
                                        <div className="form-group" key={i}>
                                            <label className="col-md-3 control-label">{el.label}</label>

                                            <div className="col-md-9">
                                                <input type={el.type} className="form-control" ref={el.ref} placeholder={el.placeholder} defaultValue={el.value} />
                                            </div>
                                        </div>
                                    )
                                })
                            }

                            { this.state.error &&
                                <div className="form-group alert-error" style={{margin: '10px 0'}}>
                                    <div className="alert alert-danger" role="alert">
                                        <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                        <span>{this.state.error}</span>
                                    </div>
                                </div>
                            }

                            <div className="form-group">
                                <div className="col-md-offset-3 col-md-9">
                                    <button type="button" disabled={this.state.loading} className="btn btn-info" onClick={this.onButtonSignUpClick.bind(this)}>
                                        <i className="icon-hand-right"></i> Sign Up
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegistrationForm;