import React from 'react';
import { Navigation, State } from 'react-router';
import RegistrationForm from 'auth/view/registration-form';
import AuthService from 'auth/service/auth';
import User from 'auth/model/user';

var service = new AuthService();

export default React.createClass({

    mixins: [Navigation, State],

    handleRegister(user, password){

        service.login(user.login, password, () => {

            service.check((data) => {

                User.current.setId(data.id);
                User.current.setFullName(data.fullName);
                User.current.setRoles(data.roles);

                this.transitionTo('/');
            });
        });
    },

    handleToggleAuth(){
        this.transitionTo('/login');
    },

    render(){

        var fields = [
            { ref: 'fullName', type: 'text', label: 'Full Name', placeHolder: 'Full Name' },
            { ref: 'email', type: 'email', label: 'Email', placeHolder: 'Email Address' },
            { ref: 'login', type: 'text', label: 'Login', placeHolder: 'Login' },
            { ref: 'password', type: 'password', label: 'Password', placeHolder: 'Password' }
        ];

        return (<RegistrationForm fields={fields} onRegister={this.handleRegister} onButtonToggleAuthClick={this.handleToggleAuth} />)
    }
});

