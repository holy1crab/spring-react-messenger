let singleton = Symbol();
let singletonEnforcer = Symbol();

class User {

    constructor(enforcer) {
        if(singletonEnforcer !== enforcer) throw new Error('invalid call');

        this.id = 0;
        this.roles = [];
        this.fullName = '';
    }

    static get current() {
        if(!this[singleton]) {
            this[singleton] = new User(singletonEnforcer);
        }
        return this[singleton];
    }

    isAuthenticated(){
        return !!this.id;
    }

    getId(){
        return this.id;
    }

    setId(val){
        this.id = val;
    }

    setRoles(roles){
        this.roles = roles;
    }

    hasRole(role){
        return this.roles.filter(x => x === role).length > 0;
    }

    getFullName(){
        return this.fullName;
    }

    setFullName(val){
        this.fullName = val;
    }
}

export default User;