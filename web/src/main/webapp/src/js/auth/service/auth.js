import Service from 'service';
import User from 'auth/model/user';
import EventBus from 'util/simple-event-bus';

class AuthService extends Service {

    constructor(){
        super();
    }

    check(callback){

        this.request('/auth/check', {
            success: function(response){
                callback(response.data);
            }
        })
    }

    login(username, password, callback){
        this.request('/auth/login', {
            method: 'POST',
            data: { username: username, password: password },
            completed: callback,
            success: (response) => {
                callback(response.success === 1);

                if(response.success){
                    EventBus.instance.trigger('logged:in', response);
                }
            }
        })
    }

    logout(callback){
        this.request('/auth/logout', {
            success: callback
        });
    }

    register(options, callback){
        this.request('/auth/register', {
            method: 'POST',
            data: options.data,
            success: callback
        });
    }

    changePassword(oldPassword, newPassword, callback){

        this.request('/user/' + User.current.getId() + '/password/change', {
            method: 'PUT',
            data: {
                oldPassword: oldPassword,
                newPassword: newPassword
            },
            success: callback
        });
    }
}

export default AuthService;
