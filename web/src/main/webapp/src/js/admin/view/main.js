import React from 'react';
import AdminService from 'admin/service/admin'
import AuthService from 'auth/service/auth'

var adminService = new AdminService();
var authService = new AuthService();

var Admin = React.createClass({

    getInitialState() {
        return {
            add: false,
            users: [],
            error: ''
        };
    },

    refreshUsers() {

        adminService.getAll((response) => {

            var userRoles = {};

            for(let i = 0, len = response.data.roles.length; i < len; ++i){

                let userRole = response.data.roles[i];

                if(!userRoles.hasOwnProperty(userRole.userId)){
                    userRoles[userRole.userId] = [];
                }

                userRoles[userRole.userId].push(userRole.role.name);
            }

            response.data.users.forEach((el) => {
                el.roles = userRoles[el.id] || [];
            });

            this.setState({ users: response.data.users, add: false })
        });
    },

    componentDidMount(){
        this.refreshUsers();
    },

    handleAdminToggle(el, e){

        var checked = e.currentTarget.checked;

        (checked) ? el.roles.push('ROLE_ADMIN') : el.roles.splice(el.roles.indexOf('ROLE_ADMIN'), 1);

        this.setState({ users: this.state.users });

        adminService.toggleRoles(checked ? AdminService.PUT : AdminService.DELETE, el.id, ['ROLE_ADMIN'], (response) => {

            if(!response.success){

                (checked) ? el.roles.splice(el.roles.indexOf('ROLE_ADMIN'), 1) : el.roles.push('ROLE_ADMIN');

                this.setState({ users: this.state.users });

                alert(response.error.message);
            }
        });
    },

    handleButtonAddClick(){
        this.setState({ add: true });
    },

    handleCancelAdd(){
        this.setState({ add: false });
    },

    handleSave(){

        var values = {};

        ['fullName', 'email', 'login', 'password'].forEach((el) => {
            values[el] = React.findDOMNode(this.refs[el]).value;
        });

        authService.register({ data: values }, (response) => (response.success) ? this.refreshUsers() : this.setState({ error: response.error.message }));
    },

    handleRemove(el){
        adminService.removeUser(el.id, (response) => (response.success) ? this.refreshUsers() : this.setState({ error: response.error.message }));
    },

    render(){

        var users = this.state.users.map((el) => {
            return (
                <tr>
                    <td>{el.fullName}</td>
                    <td>{el.email}</td>
                    <td>{el.login}</td>
                    <td>{el.roles.join(', ')}</td>
                    <td><input type="checkbox" checked={el.roles.indexOf('ROLE_ADMIN') !== -1} onChange={this.handleAdminToggle.bind(this, el)} /></td>
                    <td><button className="btn glyphicon glyphicon-remove" title="remove user" onClick={this.handleRemove.bind(this, el)}></button></td>
                </tr>
            )
        });

        var footer = (this.state.add &&
            <tfoot>
                <tr>
                    <td><input type="text" className="form-control" placeholder="full name" ref="fullName" /></td>
                    <td><input type="email" className="form-control" placeholder="email" ref="email" /></td>
                    <td><input type="login" className="form-control" placeholder="login" ref="login" /></td>
                    <td><input type="password" className="form-control" placeholder="password" ref="password" /></td>
                    <td colSpan="2">
                        <button type="button" className="btn" onClick={this.handleCancelAdd}>cancel</button>
                        <button type="button" className="btn" onClick={this.handleSave} style={{marginLeft: 5}}>save</button>
                    </td>
                </tr>
            </tfoot>);

        return (
            <div>
                {
                (this.state.users.length) &&
                    <table className="table table-striped table-hover" style={{ tableLayout: 'fixed' }}>
                        <thead>
                            <tr>
                                <th>ФИО</th>
                                <th>email</th>
                                <th>login</th>
                                <th>roles</th>
                                <th>is admin</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                         {users}
                        </tbody>
                        {footer}
                    </table>
                }

                { this.state.error &&
                <div className="form-group alert-error" style={{margin: '10px 0'}}>
                    <div className="alert alert-danger" role="alert">
                        <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span>{this.state.error}</span>
                    </div>
                </div>
                }

                <button className="btn" onClick={this.handleButtonAddClick}>+</button>
            </div>
        )
    }
});

export default Admin;