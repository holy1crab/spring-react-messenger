import Service from 'service';
import User from 'auth/model/user';

export default class AdminService extends Service {

    static get PUT(){ return 'PUT' };
    static get DELETE(){ return 'DELETE' };

    getAll(callback) {

        this.request('/user/all', {
            success: callback
        });
    }

    removeUser(userId, callback) {
        this.request('/user/' + userId, {
            method: 'DELETE',
            success: callback
        });
    }

    toggleRoles(method, userId, roles, callback) {

        var url = '/user/' + userId + '/role';
        var data;

        (method === AdminService.PUT)
            ? data = { roles: roles.join(',') }
            : url += '/' + roles.join(',');

        this.request(url, {
            data: data,
            method: method,
            success: callback
        })
    }
}