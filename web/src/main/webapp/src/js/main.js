import React from 'react';
import Router, { Route, Link, Navigation, Redirect } from 'react-router';
import { history } from 'react-router/lib/BrowserHistory';
import InboxView from 'inbox/view/main';
import InboxMessageDetailsView from 'inbox/view/message-details';
import InboxMessageNew from 'inbox/view/message-new';
import RegistrationView from 'auth/view/registration-view';
import PasswordResetForm from 'auth/view/password-reset-form';
import LoginView from 'auth/view/login-view';
import AdminView from 'admin/view/main';
import AuthService from 'auth/service/auth';
import MessageService from 'inbox/service/message';
import User from 'auth/model/user';
import Modal from 'ui/modal';
import {appendBody} from 'util/dom';
import EventBus from 'util/simple-event-bus';
import config from 'config';

function requireAuth(nextState, transition) {
    if (!User.current.isAuthenticated()){
        transition.to('login', { next: nextState.location.pathname }, null);
    }
}

function requireAdmin(nextState, transition) {
    if(!User.current.hasRole('ROLE_ADMIN')){
        transition.to('login', { next: nextState.location.pathname }, null);
    }
}

var authService = new AuthService();
var messageService = new MessageService();

var preloadData = function(callback){

    authService.check(function(data){

        User.current.setId(data.id);
        User.current.setFullName(data.fullName);
        User.current.setRoles(data.roles);

        callback();
    })
};

preloadData(onDataLoaded);

function onDataLoaded(){

    var App = React.createClass({

        mixins: [ Navigation ],

        getInitialState() {
            return {
                authenticated: User.current.isAuthenticated(),
                messageCount: null
            };
        },

        componentDidMount(){

            EventBus.instance.on('logged:in', () => {
                this.setState({authenticated: true});
            });

            EventBus.instance.on('messages:received', (options) => {
                this.setState({ messageCount: options.data.length });
            });
        },

        logout(e){

            e.preventDefault();

            authService.logout(() => {

                User.current.setId(0);
                User.current.setRoles([]);
                User.current.setFullName('');

                // this.transitionTo('/');

                // kludge. transitionTo don't handle onEnter
                requireAuth({ location: '/' }, { to: this.transitionTo });

                this.setState({ authenticated: false, messageCount: null });
            });
        },

        onButtonToggleChangePasswordModal(){
            React.render(<PasswordResetForm />, appendBody());
        },

        render() {

            let userHeader;

            if(this.state.authenticated){
                userHeader = (
                    <div>
                        <span style={{paddingRight: 10}}>Logged in as: {User.current.getFullName()}</span>
                        {User.current.hasRole('ROLE_ADMIN') && <Link to="/admin" className="btn">Управление пользователями</Link>}
                        <button type="button" className="btn" onClick={this.onButtonToggleChangePasswordModal}>Сменить пароль</button>
                        <button type="button" className="btn" onClick={this.logout}>Выйти</button>
                    </div>
                );
            } else {
                userHeader = (
                    <div>
                        <Link to="/login" className="btn">Sign in</Link>
                        <Link to="/register" className="btn">Sign up</Link>
                    </div>
                )
            }

            return (
                <div className="container">

                    <div className="header clearfix">
                        <nav>
                            <ul className="nav nav-pills pull-right">
                                <li role="presentation">{userHeader}</li>
                            </ul>
                        </nav>

                        <h3 className="text-muted">Messenger App</h3>

                        <div className="nav">
                            <Link to="/inbox">Inbox {this.state.messageCount !== null && <span className="badge">{this.state.messageCount}</span>}</Link>
                        </div>

                        <div>
                            {this.props.children}
                        </div>

                        <div className="clearfix"></div>
                    </div>
                </div>
            );
        }
    });

    var NoMatch = React.createClass({

        render(){
            return (<div style={{fontSize: '100px'}}>404</div>)
        }
    });

    React.render((
        <Router history={history}>
            <Route component={App}>
                <Route path="/inbox" component={InboxView} onEnter={requireAuth}>
                    <Route path="/new" component={InboxMessageNew} />
                    <Route path="/:id" component={InboxMessageDetailsView} />
                </Route>
                <Route path="/login" component={LoginView}/>
                <Route path="/register" component={RegistrationView}/>
                <Route path="/admin" component={AdminView} onEnter={requireAdmin} />

                <Redirect from="/" to="/inbox" />

                <Route path="/*" component={NoMatch} />
            </Route>
        </Router>
    ), document.body);
}

