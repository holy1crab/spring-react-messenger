let singleton = Symbol();
let singletonEnforcer = Symbol();

export default class EventBus {

    constructor(enforcer){

        if(enforcer != singletonEnforcer) throw new Error("invalid call");

        this.events = {};
    }

    static get instance() {
        if(!this[singleton]) {
            this[singleton] = new EventBus(singletonEnforcer);
        }
        return this[singleton];
    }

    on(path, method){
        if(!this.events.hasOwnProperty(path)){
            this.events[path] = [];
        }

        this.events[path].push(method);
    }

    trigger(path, options){

        var methods = this.events[path] || [];

        methods.forEach(method => method(options));
    }

}
