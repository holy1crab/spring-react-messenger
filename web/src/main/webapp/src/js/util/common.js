
export function dateFormat(date){

    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    return [two(day), '.', two(month), '.', year,
        date.getHours() > 0 || date.getMinutes() > 0 ? ' ' + two(date.getHours()) + ':' + two(date.getMinutes()) : ''].join('')
}

function two(v){
    return v < 9 ? '0' + v : v;
}