import React from 'react';
import {appendBody} from '../util/dom';

export default React.createClass({

    handleButtonCloseClick(e){

        if(typeof this.props.onRequestClose === 'function'){
            this.props.onRequestClose(e);
        } else {
            this.getDOMNode().parentNode.removeChild(this.getDOMNode());
        }
    },

    render(){
        return (
            <div>
                <div className="modal fade in" style={{ display: 'block' }}>
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.handleButtonCloseClick}>
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 className="modal-title">{this.props.headerText}</h4>
                            </div>

                            <div className="modal-body">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade" style={{opacity: 0.5}} onClick={this.handleButtonCloseClick}></div>
            </div>
        )
    }

});