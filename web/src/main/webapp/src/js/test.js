import React from 'react';
import Router, { Route, Link, Navigation, Redirect } from 'react-router';
import LinkedStateMixin from 'react/lib/LinkedStateMixin';
import Modal from './ui/modal';
import {appendBody} from './util/dom'
import BrowserHistory, { history } from 'react-router/lib/BrowserHistory';
import PasswordResetForm from 'auth/view/password-reset-form'

var Abc = {

    componentDidMount(){
        console.log('did mount from mixin');
    }

};

var Test = React.createClass({

    mixins: [Abc, LinkedStateMixin],

    onButtonClick(){
        React.render(this.renderModal(), appendBody());
    },

    componentDidMount(){



        console.log('did mount');
    },

    componentDidUpdate(){
        console.log('did update');
    },

    renderModal(){
        return (
            <Modal headerText="test header" onRequestClose={this.onRequestClose}>
                <h1>hello from modal</h1>
                <input type="password" ref="testInput" />

                <button type="button" onClick={this.onModalButtonClick} className="btn btn-default">click</button>
            </Modal>
        )
    },

    onModalButtonClick(e){
        console.log(e, this.refs.testInput);
    },

    onButtonRefClick(){
        console.log(this.refs);
    },

    handleChange(el, e){
        console.log(e.target.checked);

        el.checked = e.target.checked;

        this.setState({rows: window.rows});

        console.log(window.rows);
    },

    getInitialState(){

        var rows = [
            { checked: true },
            { checked: false },
            { checked: false },
            { checked: true }
        ];

        window.rows = rows;

        return { rows: rows }
    },

    render(){


        console.log('render');

        return (
            <div style={{ margin: 50 }}>

                {
                    rows.map(x => {

                        return <div><input type="checkbox" checked={x.checked} onChange={this.handleChange.bind(this, x)}/></div>
                    })
                }

                <button onClick={this.onButtonClick} className="btn btn-default">show me the money</button>

                <Link to="/password/reset">change password</Link>

                {
                    ['a', 'b', 'c'].map((el) => {
                        return <div ref={el}>{el}</div>
                    })
                }

                <button onClick={this.onButtonRefClick} className="btn btn-default">click</button>

                {this.props.children}
            </div>
        )
    }
});

function onPasswordResetEnter(nextState, transition){
    console.log(arguments);
}

React.render((
    <Router history={history}>
        <Route path="/" component={Test}>
            <Route path="/password/reset" component={PasswordResetForm} onEnter={onPasswordResetEnter}/>
        </Route>
    </Router>
), document.body);

//var routes = (
//    <Route path="/" component={Test}>
//        <Route path="/password/reset" component={PasswordResetForm} onEnter={onPasswordResetEnter}/>
//    </Route>
//);
//console.log(BrowserHistory);
//Router.run(routes, new BrowserHistory(), function (Handler) {
//    React.render(<Handler/>, document.body);
//});

//Router.run(function(Handler, state) {
//    React.render(<Handler prevPath={prevPath} />, document.body);
//    prevPath = state.path;
//});

//React.render((<Test />), document.body);