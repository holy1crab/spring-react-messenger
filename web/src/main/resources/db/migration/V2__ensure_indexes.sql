-- Index: contact_list_contact_id_idx

-- DROP INDEX contact_list_contact_id_idx;

CREATE INDEX contact_list_contact_id_idx
ON contact_list
USING btree
(contact_id);

-- Index: contact_list_user_id_idx

-- DROP INDEX contact_list_user_id_idx;

CREATE INDEX contact_list_user_id_idx
ON contact_list
USING btree
(user_id);

-- Index: message_owner_id_idx

-- DROP INDEX message_owner_id_idx;

CREATE INDEX message_owner_id_idx
ON message
USING btree
(owner_id);

-- Index: message_recipient_id_idx

-- DROP INDEX message_recipient_id_idx;

CREATE INDEX message_recipient_id_idx
ON message
USING btree
(recipient_id);

-- Index: user_role_role_id_idx

-- DROP INDEX user_role_role_id_idx;

CREATE INDEX user_role_role_id_idx
ON user_role
USING btree
(role_id);

-- Index: user_role_user_id_idx

-- DROP INDEX user_role_user_id_idx;

CREATE INDEX user_role_user_id_idx
ON user_role
USING btree
(user_id);