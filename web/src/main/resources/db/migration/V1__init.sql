CREATE TABLE "user"
(
  id serial NOT NULL,
  login character varying(128),
  hashed_password character varying(1024),
  email character varying(128),
  full_name character varying(512),
  creation_date timestamp without time zone DEFAULT now(),
  deleted boolean NOT NULL DEFAULT false,
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_email_key UNIQUE (email),
  CONSTRAINT user_username_key UNIQUE (login)
);

CREATE TABLE role
(
  id serial NOT NULL,
  name character varying(64),
  CONSTRAINT role_pkey PRIMARY KEY (id)
);

CREATE TABLE message
(
  id serial NOT NULL,
  text text,
  owner_id integer,
  recipient_id integer,
  creation_date timestamp without time zone DEFAULT now(),
  deleted boolean DEFAULT false,
  subject character varying(1024),
  CONSTRAINT message_pkey PRIMARY KEY (id),
  CONSTRAINT message_owner_id_fkey FOREIGN KEY (owner_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT message_recipient_id_fkey FOREIGN KEY (recipient_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE contact_list
(
  user_id integer NOT NULL,
  contact_id integer NOT NULL,
  CONSTRAINT contact_list_pkey PRIMARY KEY (user_id, contact_id),
  CONSTRAINT contact_list_contact_id_fkey FOREIGN KEY (contact_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT contact_list_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE user_role
(
  user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id),
  CONSTRAINT user_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES role (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT user_role_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES "user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO role("name") VALUES('ROLE_ADMIN');
INSERT INTO "user" (login, hashed_password, email, full_name) VALUES ('admin', '$2a$10$Hix5gQa08Jpldd9ZEGFsEeZBoifoTlicRbkZQr7uoYNCruuQKNWeO', 'admin@admin', 'admin');
INSERT INTO user_role(user_id, role_id) VALUES((SELECT id FROM "user" WHERE login = 'admin'), (SELECT id FROM "role" WHERE "name" = 'ROLE_ADMIN'));

