package com.messenger.response;


import java.util.List;

class AuthData {

    public Integer id;
    public String fullName;
    public List<String> roles;

    public AuthData(Integer id, String fullName, List<String> roles){
        this.id = id;
        this.fullName = fullName;
        this.roles = roles;
    }
}

public class AuthResponse extends Response<AuthData> {

    public AuthResponse(Integer id, String fullName, List<String> roles){
        super(new AuthData(id, fullName, roles));
    }
}
