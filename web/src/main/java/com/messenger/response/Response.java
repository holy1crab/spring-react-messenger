package com.messenger.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {

    private int success = 1;
    private T data;

    public Response(T data){
        this.data = data;
    }

    public Response(){}

    public Response(int success){
        this.success = success;
    }

    public Response(int success, T data){
        this.success = success;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }
}
