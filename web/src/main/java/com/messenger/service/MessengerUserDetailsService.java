package com.messenger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

@Service
public class MessengerUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Transactional()
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        com.messenger.model.User user = userService.getByLogin(username);

        if(user == null){
            throw new UsernameNotFoundException("username " + username + " doesn't exists");
        }

        List<String> roles = userService.getRoles(user);

        HashSet<GrantedAuthority> authorities = new HashSet<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

        roles.forEach(s -> authorities.add(new SimpleGrantedAuthority(s)));

        return new MessengerUser(user.getId(), user.getLogin(), user.getHashedPassword(), user.getFullName(), authorities);
    }
}
