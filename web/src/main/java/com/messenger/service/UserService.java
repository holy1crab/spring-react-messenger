package com.messenger.service;

import com.messenger.model.Role;
import com.messenger.model.User;
import com.messenger.model.UserRole;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Repository
@Transactional
public class UserService extends BaseService<User, Integer> {

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    public User getByLogin(String login){

        List<User> users = getCurrentSession().createQuery("FROM User u WHERE u.login = :login AND deleted is false").setString("login", login).list();

        if(users.size() == 0){
            return null;
        }

        return users.get(0);
    }

    public List<?> getList(){
        return getCurrentSession().createQuery("FROM User WHERE deleted is false ORDER BY creationDate ASC").list();
    }

    public List<?> getContactsAvailable() {
        return getCurrentSession().createQuery("SELECT new Map(u.id as id, u.fullName as fullName) " +
                "FROM User u WHERE deleted is false ORDER BY creationDate ASC").list();
    }

    public List<String> getRoles(User user){

        List<String> res = new ArrayList<>();

        List<?> roles = getCurrentSession().createQuery("FROM UserRole where userId = :userId").setParameter("userId", user.getId()).list();

        roles.forEach(o -> res.add(((UserRole)o).getRole().getName()));

        return res;
    }

    public List<Role> getRoles(String[] roles){
        return getCurrentSession().createQuery("FROM Role WHERE name IN (:roles)").setParameterList("roles", roles).list();
    }

    public Serializable save(User user){
        return getCurrentSession().save(user);
    }

    public void addUserRole(int userId, int roleId){
        getCurrentSession().saveOrUpdate(new UserRole(userId, roleId));
    }

    public void deleteUserRole(UserRole entity){
        getCurrentSession().delete(entity);
    }

    public List<?> getUserRoles(){
        return getCurrentSession().createQuery("FROM UserRole").list();
    }
}
