package com.messenger.service;

import java.util.*;
import java.util.stream.Collectors;

import com.messenger.model.Message;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class MessageService extends BaseService<Message, Integer> {

    @Override
    protected Class<Message> getEntityClass() {
        return Message.class;
    }

    public List<?> filter(Map<String, Object> filter, Map<String, String> mapping, SortedMap<String, Integer> sort){

        String cond = "";
        Map<String, Object> params = new HashMap<>();

        if(filter != null){
            if(filter.containsKey("recipientId")){
                cond += "AND recipientId = :recipientId";
                params.put("recipientId", filter.get("recipientId"));
            }
        }

        List<String> m = mapping.entrySet()
                .stream().map(entry -> "m." + entry.getKey() + " as " + entry.getValue()).collect(Collectors.toList());

        List<String> sorts = sort.entrySet()
                .stream().map(entry -> String.format(" %s %s", entry.getKey(), entry.getValue() > 0 ? "ASC" : "DESC")).collect(Collectors.toList());

        String q = "select new Map(" + String.join(",", m) + ") from Message m where 1=1 " + cond + " AND deleted is not true";

        if(sorts.size() > 0){
            q += String.format(" ORDER BY %s", String.join(",", sorts));
        }

        Query query = getCurrentSession().createQuery(q);

        params.entrySet().forEach(entry -> query.setParameter(entry.getKey(), entry.getValue()));

        return query.list();
    }
}
