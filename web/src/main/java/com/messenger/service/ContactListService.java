package com.messenger.service;

import com.messenger.model.ContactList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ContactListService extends BaseService<ContactList, Integer> {

    @Override
    protected Class<ContactList> getEntityClass() {
        return ContactList.class;
    }

    public List<?> getContacts(int userId){

        return getCurrentSession().createQuery("SELECT new Map(cl.contact.id as id, cl.contact.fullName as fullName) " +
                "FROM ContactList cl WHERE userId = :userId").setParameter("userId", userId).list();
    }
}
