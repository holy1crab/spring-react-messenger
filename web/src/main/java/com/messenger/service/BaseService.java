package com.messenger.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Repository
@Transactional
public abstract class BaseService<T, PK extends Serializable> {

    @Autowired
    SessionFactory sessionFactory;

    protected Session getCurrentSession(){
        return sessionFactory.getCurrentSession();
    }

    protected abstract Class<T> getEntityClass();

    public T get(PK id){
        return (T) getCurrentSession().get(getEntityClass(), id);
    }

    public void update(T entity){
        getCurrentSession().update(entity);
    }

    public void delete(T entity){
        getCurrentSession().delete(entity);
    }

    public Serializable save(T entity){
        return getCurrentSession().save(entity);
    }

}
