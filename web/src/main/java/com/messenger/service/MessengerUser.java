package com.messenger.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class MessengerUser extends User {

    private int id;
    private String fullName;

    public MessengerUser(int id, String username, String password, String fullName, Collection<? extends GrantedAuthority> authorities){
        super(username, password, authorities);
        this.id = id;
        this.fullName = fullName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean hasRole(String role){

        for (GrantedAuthority authority : this.getAuthorities()) {
            if(authority.getAuthority().equals(role)){
                return true;
            }
        }

        return false;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
