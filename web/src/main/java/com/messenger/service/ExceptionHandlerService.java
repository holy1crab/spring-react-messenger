package com.messenger.service;

import com.messenger.exception.BaseException;
import com.messenger.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlerService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler({ NoHandlerFoundException.class })
    protected ModelAndView noFound(){
        return new ModelAndView("index");
    }

    @ExceptionHandler({ BaseException.class })
    @ResponseBody
    protected Map<String, Object> handleInvalidRequest(Exception e, HttpServletRequest request, HttpServletResponse response) {

        ErrorResponse errorResponse = new ErrorResponse();
        Map<String, Object> res = new HashMap<>();
        HttpStatus status = HttpStatus.OK;

        errorResponse.message = e.getMessage();

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(status.value());

        logger.error("{}", e.getMessage());

        res.put("result", 0);
        res.put("error", errorResponse);

        return res;
    }
}
