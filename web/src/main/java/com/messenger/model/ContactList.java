package com.messenger.model;

import javax.persistence.*;
import java.io.Serializable;

class ContactListId implements Serializable {

    protected int userId;
    protected int contactId;

    public ContactListId() {}

    public ContactListId(int userId, int contactId) {
        this.userId = userId;
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactListId that = (ContactListId) o;

        return contactId == that.contactId && userId == that.userId;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + contactId;
        return result;
    }
}

@Entity
@Table(name = "contact_list")
@IdClass(ContactListId.class)
public class ContactList {

    @Column(name = "user_id")
    @Id
    private int userId;

    @Column(name = "contact_id")
    @Id
    private int contactId;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", insertable = false, updatable = false)
    private User contact;

    public ContactList(){}

    public ContactList(int userId, int contactId){
        this.userId = userId;
        this.contactId = contactId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }
}
