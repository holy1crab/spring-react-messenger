package com.messenger.model;

import javax.persistence.*;
import java.io.Serializable;

class UserRoleId implements Serializable {

    int userId;
    int roleId;

    public UserRoleId() {}

    public UserRoleId(int userId, int roleId) {
        this.userId = userId;
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRoleId that = (UserRoleId) o;

        return userId == that.userId && roleId == that.roleId;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + roleId;
        return result;
    }
}

@Entity
@Table(name = "user_role")
@IdClass(UserRoleId.class)
public class UserRole {

    public static final String ADMIN = "ROLE_ADMIN";

    @Id
    @Column(name = "user_id")
    private int userId;

    @Id
    @Column(name = "role_id")
    private int roleId;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", insertable = false, updatable = false)
    private Role role;

    public UserRole(){}

    public UserRole(int userId, int roleId){
        this.userId = userId;
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role owner) {
        this.role = owner;
    }
}
