package com.messenger.form;

public class RolesForm {

    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String[] getRolesAsArray(){
        return roles.split(",");
    }
}
