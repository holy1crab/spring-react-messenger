package com.messenger.exception;

public class InvalidPassword extends BaseException {

    public InvalidPassword(){
        super("invalid password");
    }
}
