package com.messenger.exception;

public class BadCredential extends BaseException {

    public BadCredential(){
        super("bad credentials");
    }
}
