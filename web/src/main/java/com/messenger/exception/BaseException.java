package com.messenger.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.OK)
public class BaseException extends RuntimeException {

    public BaseException(){}

    public BaseException(String message){
        super(message);
    }
}
