package com.messenger.exception;

public class AccessDenied extends BaseException {

    public AccessDenied(String message){
        super(message);
    }
}
