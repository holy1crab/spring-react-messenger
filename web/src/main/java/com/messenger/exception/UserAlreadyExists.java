package com.messenger.exception;

public class UserAlreadyExists extends BaseException {

    public UserAlreadyExists(){
        super("user already exists");
    }

}
