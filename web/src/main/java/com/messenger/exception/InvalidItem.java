package com.messenger.exception;

public class InvalidItem extends BaseException {

    public InvalidItem(String message){
        super(message);
    }

    public InvalidItem(){
        super("invalid item");
    }
}
