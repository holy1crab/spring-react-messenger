package com.messenger.controller;

import javax.servlet.http.HttpServletRequest;

import com.messenger.exception.AccessDenied;
import com.messenger.exception.InvalidPassword;
import com.messenger.form.ChangePasswordForm;
import com.messenger.form.LoginForm;
import com.messenger.form.RolesForm;
import com.messenger.model.Role;
import com.messenger.model.User;
import com.messenger.model.UserRole;
import com.messenger.response.Response;
import com.messenger.service.MessengerUser;
import com.messenger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "{userId}/password/change", method = RequestMethod.PUT)
    @PreAuthorize("isAuthenticated()")
    public Response changePassword(HttpServletRequest request, @AuthenticationPrincipal MessengerUser principal,
                               @PathVariable int userId, @ModelAttribute ChangePasswordForm form) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if(principal.getId() != userId && !request.isUserInRole("ROLE_ADMIN")){
            throw new AccessDenied("invalid user");
        }

        User user = userService.get(userId);

        if(!passwordEncoder.matches(form.getOldPassword(), user.getHashedPassword())){
            throw new InvalidPassword();
        }

        String hashedPassword = passwordEncoder.encode(form.getNewPassword());

        user.setHashedPassword(hashedPassword);

        userService.update(user);

        return new Response<>();
    }

    @RequestMapping(value = "/contact/available", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public Response getContactsAvailable() {

        return new Response<>(new Object(){
            public final List<?> available = userService.getContactsAvailable();
        });
    }

    @RequestMapping(value = "/{userId}/role", method = RequestMethod.PUT)
    @Secured("ROLE_ADMIN")
    public Response addRole(@PathVariable int userId, @ModelAttribute RolesForm form){

        List<Role> roles = userService.getRoles(form.getRolesAsArray());

        for(Role role: roles){
            userService.addUserRole(userId, role.getId());
        }

        return new Response<>();
    }

    @RequestMapping(value = "/{userId}/role/{roles}", method = RequestMethod.DELETE)
    @Secured("ROLE_ADMIN")
    public Response removeRole(@PathVariable int userId, @PathVariable String roles){

        List<Role> rolesList = userService.getRoles(roles.split(","));

        for(Role role: rolesList){
            userService.deleteUserRole(new UserRole(userId, role.getId()));
        }

        return new Response<>();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public Response getUsers(){

        return new Response<>(new Object() {
            public final List<?> users = userService.getList();
            public final List<?> roles = userService.getUserRoles();
        });
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    @Secured("ROLE_ADMIN")
    public Response delete(@PathVariable int userId){

        User user = userService.get(userId);

        user.setDeleted(true);

        userService.update(user);

        return new Response<>();
    }
}
