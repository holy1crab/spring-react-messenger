package com.messenger.controller;

import com.messenger.exception.InvalidItem;
import com.messenger.form.MessageForm;
import com.messenger.model.Message;
import com.messenger.model.UserRole;
import com.messenger.response.Response;
import com.messenger.service.MessageService;
import com.messenger.service.MessengerUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public Response filter(@AuthenticationPrincipal MessengerUser principal, @RequestParam(required = false, defaultValue = ",") String sort){

        Map<String, Object> filters = new HashMap<>();

        Map<String, String> mapping = new HashMap<>();

        mapping.put("id", "id");
        mapping.put("creationDate", "creationDate");
        mapping.put("subject", "subject");
        mapping.put("text", "text");
        mapping.put("owner.fullName", "ownerFullName");

        if(principal.hasRole("ROLE_ADMIN")){
            mapping.put("recipient.fullName", "recipientFullName");
        } else {
            filters.put("recipientId", principal.getId());
        }

        String[] sortChunks = sort.split(",", 0);

        SortedMap<String, Integer> sortedMap = new TreeMap<>();

        for(int i = 0, len = sortChunks.length; i < len; i+=2){
            sortedMap.put(sortChunks[i], Integer.parseInt(sortChunks[i + 1]));
        }

        return new Response<List<?>>(messageService.filter(filters, mapping, sortedMap));
    }

    @RequestMapping(value = "{messageId}/delete", method = RequestMethod.DELETE)
    @PreAuthorize("isAuthenticated()")
    public Response markDeleted(@PathVariable int messageId, @AuthenticationPrincipal MessengerUser principal){

        Message message = messageService.get(messageId);

        if(message == null || principal.getId() != message.getRecipientId() && !principal.hasRole(UserRole.ADMIN)){
            throw new InvalidItem();
        }

        message.setDeleted(true);

        messageService.update(message);

        return new Response();
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public Response send(@AuthenticationPrincipal MessengerUser principal, @ModelAttribute MessageForm form){

        Message message = new Message();

        message.setOwnerId(principal.getId());
        message.setRecipientId(form.getRecipientId());
        message.setSubject(form.getSubject());
        message.setText(form.getText());
        message.setCreationDate(new Date());

        messageService.save(message);

        return new Response();
    }

}
