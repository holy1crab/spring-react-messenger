package com.messenger.controller;

import com.messenger.form.ContactForm;
import com.messenger.model.ContactList;
import com.messenger.model.User;
import com.messenger.response.Response;
import com.messenger.service.ContactListService;
import com.messenger.service.MessengerUser;
import com.messenger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/contact")
public class ContactListController {

    @Autowired
    ContactListService contactListService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public void saveContact(@AuthenticationPrincipal MessengerUser principal, @RequestBody Map<String, Object> body){

        Integer contactId = (Integer)body.get("contact_id");

        User contactUser = userService.get(contactId);

        ContactList entity = new ContactList(principal.getId(), contactUser.getId());

        contactListService.save(entity);
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    @PreAuthorize("isAuthenticated()")
    public void deleteContact(@AuthenticationPrincipal MessengerUser principal, @RequestBody Map<String, Object> body){

        Integer contactId = (Integer)body.get("contact_id");

        User contactUser = userService.get(contactId);

        contactListService.delete(new ContactList(principal.getId(), contactUser.getId()));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public Response getContacts(@AuthenticationPrincipal MessengerUser principal){

        return new Response<>(new Object(){
            public List<?> contacts = contactListService.getContacts(principal.getId());
        });
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public Response add(@AuthenticationPrincipal MessengerUser principal, @ModelAttribute ContactForm form){
        contactListService.save(new ContactList(principal.getId(), form.getContactId()));
        return new Response<>();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public Response delete(@AuthenticationPrincipal MessengerUser principal, @ModelAttribute ContactForm form){
        contactListService.delete(new ContactList(principal.getId(), form.getContactId()));
        return new Response<>();
    }
}
