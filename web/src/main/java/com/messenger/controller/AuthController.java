package com.messenger.controller;

import com.messenger.exception.BadCredential;
import com.messenger.exception.UserAlreadyExists;
import com.messenger.form.LoginForm;
import com.messenger.model.User;
import com.messenger.response.AuthResponse;
import com.messenger.response.Response;
import com.messenger.service.MessengerUser;
import com.messenger.service.UserService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    UserService userService;

    @Autowired
    UserDetailsService userDetailsService;

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public Response getAuth(@AuthenticationPrincipal MessengerUser principal){
        List<String> roles = new ArrayList<>();
        if(principal != null) {
            principal.getAuthorities().forEach(authority -> roles.add(authority.getAuthority()));
        }
        return new AuthResponse(principal == null ? 0 : principal.getId(), principal == null ? "" : principal.getFullName(), roles);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Response login(HttpServletRequest request, @ModelAttribute LoginForm form){

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(form.getUsername(), form.getPassword());
        token.setDetails(new WebAuthenticationDetails(request));
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();

        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());

        try {
            Authentication auth = authenticationProvider.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
        } catch(BadCredentialsException e){
            throw new BadCredential();
        }

        return new Response();

    }

    @RequestMapping(value = "/logout")
    public Response logout(){
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Response();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Response register(@RequestBody MultiValueMap<String, String> body) throws UserAlreadyExists {

        final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        User userObj = new User();

        userObj.setLogin(body.getFirst("login"));
        userObj.setEmail(body.getFirst("email"));
        userObj.setFullName(body.getFirst("fullName"));
        userObj.setHashedPassword(passwordEncoder.encode(body.getFirst("password")));

        try {
            userService.save(userObj);
        } catch (ConstraintViolationException e) {
            throw new UserAlreadyExists();
        }

        return new Response<>(new Object(){
            public final User user = userObj;
        });
    }

}
