package com.messenger.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.orm.hibernate4.HibernateTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;


@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:hibernate.properties" })
public class HibernateConfig {

    @Autowired
    private Environment env;

    @Bean
    public SessionFactory sessionFactory() throws IOException {

        Properties properties = new Properties();
        properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));

        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(getDataSource());

        builder.scanPackages("com.messenger.model").addProperties(properties);

        return builder.buildSessionFactory();
    }

    @Bean
    public DataSource getDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource(env.getProperty("hibernate.connection.url"));

        dataSource.setDriverClassName(env.getRequiredProperty("hibernate.connection.driver_class"));
        dataSource.setUsername(env.getRequiredProperty("hibernate.connection.username"));
        dataSource.setPassword(env.getRequiredProperty("hibernate.connection.password"));

        return dataSource;
    }

    @Bean
    public HibernateTransactionManager txManager() throws IOException {
        return new HibernateTransactionManager(sessionFactory());
    }
}
