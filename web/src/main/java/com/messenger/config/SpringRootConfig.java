package com.messenger.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.messenger.config", "com.messenger.service" })
public class SpringRootConfig {
}