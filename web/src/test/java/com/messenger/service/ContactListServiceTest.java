package com.messenger.service;

import com.messenger.config.SpringRootConfig;
import com.messenger.config.SpringWebConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringRootConfig.class, SpringWebConfig.class})
@Transactional
public class ContactListServiceTest {

    @Autowired
    ContactListService contactListService;

    @Autowired
    SessionFactory sessionFactory;

    @Test
    public void testGetUserContacts(){

//        Session session = sessionFactory.getCurrentSession();
//
//        com.messenger.model.User user = new com.messenger.model.User();
//        user.setLogin("__test1");
//        user.setEmail("__test@test.ru");
//        user.setFullName("Jack Sparrow");

//        session.save(user);

        Assert.assertEquals("abc", "abc");
    }

}
